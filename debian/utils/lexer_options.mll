{
open Parse_options;;
open Lexing;;

let incrline lexbuf =
        lexbuf.lex_curr_p <-
                {
                        lexbuf.lex_curr_p with 
                        pos_lnum = lexbuf.lex_curr_p.pos_lnum + 1;
                        pos_bol = lexbuf.lex_curr_p.pos_cnum + 1;
                }
}

rule token = parse
| [' ' '\t']      { token lexbuf }
| '\n'           { incrline lexbuf; token lexbuf }
| ['0'-'9']+ '.' ['0'-'9']*  { FLOAT(float_of_string(lexeme lexbuf)) }
| ['0'-'9']+      { INT(Int64.of_string(lexeme lexbuf)) }
| '='        { EQUAL }
| '{'         { BEG_MOD }
| '}'        { END_MOD }
| '['        { BEG_LIST }
| ']'        { END_LIST }
| ';'         { SEP_LIST }
| "(*"         { COMMENT("(*" ^ (comment lexbuf)) }
| '('        { BEG_LIST }
| ')'        { END_LIST }
| ','        { SEP_LIST }
| '.'        { SEP_LIST }
| '"' (([^'"'] | '\\' '"')* as s) '"'   {
        String.iter (fun c -> if c = '\n' then incrline lexbuf) s;
        STRING s }
| ['A'-'Z' 'a'-'z'] ['A'-'Z' 'a'-'z' '0'-'9' '_'] * { IDENT(Lexing.lexeme lexbuf) }
| eof        { EOF }

and comment = parse
| "*)"    { "*)" }
| '*'    { "*" ^ (comment lexbuf) }
| [^'*']*  { let s = (Lexing.lexeme lexbuf) in 
    s ^ (comment lexbuf) }
