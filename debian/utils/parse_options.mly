%{

open Type_options

%}
%token EOF 
%token <float> FLOAT 
%token <int64> INT 
%token <string> COMMENT 
%token <string> STRING 
%token <string> IDENT
%token <string> CHAR
%token EQUAL 
%token BEG_MOD 
%token END_MOD 
%token BEG_LIST 
%token END_LIST 
%token SEP_LIST 
%start main
%type <Type_options.file> main
%%
main:
  COMMENT main  { $2 (* Comment ($1,$2) *) }  
| options main  { Options ($1,$2) }
| EOF    { Eof }
| END_MOD  { Eof }
;
options:
STRING EQUAL parse_options  { StringId ($1,$3) }
| IDENT EQUAL parse_options  { Id ($1,$3) }
;
parse_options:
BEG_MOD main       { ValModule $2 }
| BEG_LIST parse_list    { ValList $2 }
| IDENT        { ValIdent $1 }
| STRING      { ValString $1 }
| INT         { ValInt $1 }
| FLOAT        { ValFloat $1 }
| CHAR        { ValChar $1 }
;
parse_list:
parse_options SEP_LIST parse_list  { $1 :: $3 }
| parse_options END_LIST    { [ $1 ] }
| END_LIST        { [] }
