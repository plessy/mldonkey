Source: mldonkey
Section: net
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:
 Samuel Mimram <smimram@debian.org>,
 Mehdi Dogguy <mehdi@debian.org>
Homepage: http://mldonkey.sourceforge.net/
Build-Depends: autoconf,
 ocaml-nox,
 dh-ocaml,
 ocamlbuild,
 camlp4,
 debhelper-compat (= 13),
 po-debconf,
 xsltproc,
 zlib1g-dev,
 libbz2-dev,
 docbook-xsl,
 docbook-xml,
 m4,
 debconf,
 libnum-ocaml-dev,
 liblablgtk2-ocaml-dev,
 libgd-dev,
 liblablgtk2-gnome-ocaml-dev,
 libxml2-utils
Standards-Version: 4.5.0
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/ocaml-team/mldonkey.git
Vcs-Browser: https://salsa.debian.org/ocaml-team/mldonkey

Package: mldonkey-server
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends:
 ${shlibs:Depends},
 ${ocaml:Depends},
 ${misc:Depends},
 adduser,
 lsb-base,
 mime-support,
 debconf,
 ucf
Description: Door to the 'donkey' network
 MLDonkey is a door to the 'donkey' network, a decentralized network used to
 exchange big files on the Internet. It is written in a wonderful language,
 called Objective-Caml, and present most features of the basic Windows donkey
 client, plus some more:
  - works on UNIX-compatible platforms.
  - remote control by telnet, WEB browser or GTK+ interface.
  - access to EDonkey (edonkey2000, overnet, emule)
  - access to Gnutella1/2
  - access to Bittorrent

Package: mldonkey-gui
Architecture: any
Suggests: mldonkey-server (= ${binary:Version})
Depends:
 ${shlibs:Depends},
 ${ocaml:Depends},
 ${misc:Depends}
Description: Graphical frontend for mldonkey based on GTK+
 The GTK+ interface for mldonkey provides a convenient way of managing
 all mldonkey operations. It gives details about connected servers,
 downloaded files, friends and lets one search for files in a pleasing
 way.
